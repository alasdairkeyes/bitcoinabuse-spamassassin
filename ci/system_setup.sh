#!/bin/bash

apt update

apt upgrade -y

# Install requirements for the code
apt install spamassassin -y

# Install requirements for testing
apt install libtest-exception-perl libtest-mockobject-perl libarray-compare-perl libdevel-cover-perl make libpod-coverage-perl -y