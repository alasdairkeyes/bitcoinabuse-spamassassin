# Copyright 2018 Alasdair Keyes
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

=head1 NAME

Mail::SpamAssassin::Plugin::BitcoinAbuse - BitcoinAbuse plugin

=head1 SYNOPSIS

  loadplugin     Mail::SpamAssassin::Plugin::BitcoinAbuse

=head1 REVISION

  Revision: 1.05

=head1 DESCRIPTION

  Mail::SpamAssassin::Plugin::BitcoinAbuse is a plugin to score messages
  based on the message containing bitcoin addresses listed at
  https://www.bitcoinabuse.com/

  To find out more see
  https://gitlab.com/alasdairkeyes/bitcoinabuse-spamassassin

=head1 AUTHOR

  Alasdair Keyes <alasdair@akeyes.co.uk>

  https://www.akeyes.co.uk/

=head1 LICENSE

  http://www.apache.org/licenses/LICENSE-2.0

=cut

package Mail::SpamAssassin::Plugin::BitcoinAbuse;

use strict;
use warnings;

use Mail::SpamAssassin;
use Mail::SpamAssassin::Plugin;
use Mail::SpamAssassin::PerMsgStatus;

our @ISA = qw(Mail::SpamAssassin::Plugin);
our $VERSION = 1.05;

# Define common variables

my $config_key              = "bitcoin_abuse";
my $bitcoin_abuse_base      = 'https://www.bitcoinabuse.com/';
my $report_url              = "$bitcoin_abuse_base/api/reports/distinct";
my $default_list_filename   = "/etc/spamassassin/bitcoin_abuse_list.txt";

# Regex to detect Bitcoin addresses.
# Source https://en.bitcoin.it/wiki/Address
my $bitcoin_address_re = qr/\b((?:bc1|[13])[a-zA-HJ-NP-Z0-9]{25,39})\b/i;


sub new {
    my ($class, $mailsa) = @_;
    $class = ref($class) || $class;
    my $self = $class->SUPER::new( $mailsa );
    bless ($self, $class);

    # Register functions with Spamassassin
    $self->register_eval_rule ( 'bitcoin_abuse_check' );

    return $self;
}

# Override standard debug method by prepending it with $config_key for easier
# checking in the logs
sub dbg {
    my @message = @_;
    Mail::SpamAssassin::Plugin::dbg($config_key .': ' . (join(' ',@_) || '-'));
}


sub parse_config {
    my ($self, $opts) = @_;

    if ($opts->{ key } eq 'minimum_reports_required') {
        my $value = $opts->{ value } || '';

        if ($value !~ /^\d+$/ || $value < 1 ) {
            dbg("minimum_reports_required value $value invalid - setting to 1");
            $value = 1;
        }

        # Store
        $self->{ main }{ conf }{ $config_key }{ 'minimum_reports_required' } = $value;

        # Inform SA, we handle this option
        $self->inhibit_further_callbacks();
        return 1;
    }

    if ($opts->{ key } eq 'bitcoin_abuse_list_file') {
        $self->{ main }{ conf }{ $config_key }{ bitcoin_abuse_list_file } = $opts->{ value } || $default_list_filename;
        $self->inhibit_further_callbacks();
        return 1;
    }

    return 0;
}

sub _get_abuse_list_file_handle {
    my $self = shift;
    my $bitcoin_abuse_list_file = shift;
    open (my $fh, '<', $bitcoin_abuse_list_file)
        || die "$config_key Failed to open file '$bitcoin_abuse_list_file': $!";

    return $fh;
}

sub _extract_bitcoin_strings {
    my $self = shift;
    my $body_array = shift;
    my @bitcoin_addresses_in_email;

    foreach my $body_line (@$body_array) {
        my @line_addresses = ( $body_line =~ /$bitcoin_address_re/g );
        push (@bitcoin_addresses_in_email, @line_addresses);
    }
    return @bitcoin_addresses_in_email;
}

sub _search_for_addresses_in_email {
    my $self = shift;
    my $fh = shift;
    my $bitcoin_addresses_in_email = shift || [];

    my $found_bitcoin_addresses = {};

    # Cycle through addresses, check and increment report_count
    while (<$fh>) {
        my $line = $_;
        $line =~ s/\n+//;
        my ($bad_address, $count) = split(',', $line);
        foreach my $check_address (@$bitcoin_addresses_in_email) {
            $found_bitcoin_addresses->{ $bad_address } = $count || 0
                if (uc($check_address) eq uc($bad_address));
        }
    }
    return $found_bitcoin_addresses;
}

sub _report_count {
    my $self = shift;
    my $found_bitcoin_addresses = shift;

    my $report_count = 0;
    foreach (values(%$found_bitcoin_addresses)) {
        $report_count += $_;
    }
    return $report_count;
}

sub _update_report_description {
    my $self = shift;
    my $pms = shift;
    my $description = shift;
    my $rulename = shift;
    my $found_bitcoin_addresses = shift;

    $pms->got_hit(
        $rulename,
        "",
        description => $description . ' (' . join(',', keys(%$found_bitcoin_addresses)) . ')',
    );
}

sub bitcoin_abuse_check {
    my ($self, $pms, $body_array) = @_;

    my $minimum_reports =
            $self->{ main }{ conf }{ $config_key }{ minimum_reports_required } || 1;
    my $bitcoin_abuse_list_file =
            $self->{ main }{ conf }{ $config_key }{ bitcoin_abuse_list_file } || $default_list_filename;

    my $fh = $self->_get_abuse_list_file_handle($bitcoin_abuse_list_file);

    my @bitcoin_addresses_in_email = $self->_extract_bitcoin_strings($body_array);

    # Return if none are found
    return 0
        unless (@bitcoin_addresses_in_email);

    dbg("Requiring minimum report of $minimum_reports");

    my $found_bitcoin_addresses = $self->_search_for_addresses_in_email($fh, \@bitcoin_addresses_in_email);

    close $fh;

    my $report_count = $self->_report_count($found_bitcoin_addresses);
    dbg("Found report count of $report_count, $minimum_reports needed for positive report"); 

    if ($report_count >= $minimum_reports) {
        my $rulename = $pms->get_current_eval_rule_name();
        my $description = $pms->{ conf }->{ descriptions }->{ $rulename };

        $self->_update_report_description($pms, $description, $rulename, $found_bitcoin_addresses);

        return 1;
    }

    return 0;
}

sub default_list_filename {
    return $default_list_filename;
}

1;

=head1 METHODS

=over

=item B<new( $class, $sa )>

  Plugin constructor

  Registers the rules with SpamAssassin

=item B<parse_config( $self, $opts )>

  SpamAssassin default config parsing method.

  Loads the config settings from SpamAssassin config files

=item B<bitcoin_abuse_check( $self, $pms, $body_array )>

  Registered method

  Called by SpamAssassin to check a message against the list of
  known addresses

=item B<dbg( @message )>

  Redefine SpamAssassin's dbg function, prepends with country_filter text,
  Makes debugging easier

=item B<default_list_filename()>

  Return the default filename location for the bitcoin abuse list.

=back

=head1 PRIVATE METHODS

=over

=item B<_get_abuse_list_file_handle( $bitcoin_abust_list_file )>

  Open the provided filename and return the filehandle.

  An exception is thrown if the file cannot be opened

=item B<_extract_bitcoin_strings( $body_array )>

  Cycle through the provided array ref looking for bitcoin-type
  strings.

  Returns an array of all the matching strings

=item B<_search_for_addresses_in_email( $fh, $bitcoin_addresses_in_email )>

  Function reads through the file provided by filehandle $fh and checks
  if it contains any of the provided bitcoin addresses in
  $bitcoin_addresses_in_email

  Returns a hashref of { "bitcoinaddress" => reported_count }

=item B<_report_count( $found_bitcoin_addresses )>

  Function adds the count provided in the $found_bitcoin_addresses hash.

  Returns an integer of the count

=item B<_update_report_description( $pms, $description, $rulename, $found_bitcoin_addresses )>

  Updates the reported results and updates the rule hit information
  this message

=back

=cut
