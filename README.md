**DEPRECATION NOTE:** https://www.bitcoinabuse.com has now closed and merged with https://www.chainabuse.com/ - Their API has been taken down so this plugin is now deprecated

[![pipeline status](https://gitlab.com/alasdairkeyes/bitcoinabuse-spamassassin/badges/master/pipeline.svg)](https://gitlab.com/alasdairkeyes/bitcoinabuse-spamassassin/commits/master) [![coverage report](https://gitlab.com/alasdairkeyes/bitcoinabuse-spamassassin/badges/master/coverage.svg)](https://gitlab.com/alasdairkeyes/bitcoinabuse-spamassassin/commits/master)

# bitcoinabuse-spamassassin - DEPRECATED
Spamassassin Plugin to scan messages for Bitcoin addresses known to be used
in abuse emails (phishing, blackmail spam, etc).

This uses the data available at https://www.bitcoinabuse.com/


## Site:
https://gitlab.com/alasdairkeyes/bitcoinabuse-spamassassin


## Installation
- Copy the `30_bitcoin_abuse.cf` file into your SpamAssassin config folder
  (Usually /etc/mail/spamassassin/ or /etc/spamassassin/)
- Adjust the following values as required
  - `minimum_reports_required`: Some addresses might be added to the list
    incorrectly or maliciously. Set this value to be > 1 so that the number
    of reports against a bitcoin address is considered, not just it's
    presence in the list
  - `bitcoin_abuse_list_file`: The path of the bitcoin abuse list file
- Copy the `lib/Mail/SpamAssassin/Plugin/BitcoinAbuse.pm` file into your
  SpamAssassin plugin folder (Usually
  /usr/lib/perl5/vendor_perl/x.x.x/Mail/SpamAssassin/Plugin/ or
  /usr/share/perl5/Mail/SpamAssassin/Plugin/
- Restart SpamAssassin
- Copy the file `update_bitcoin_abuse_list.pl` to your system and setup to
  run as a cron. Once per day should be fine, the source data is only
  updated once per hour (see https://www.bitcoinabuse.com/api-docs#distinct)
  Please don't run this script too frequently, https://www.bitcoinabuse.com/
  provide this data free of charge, please respect this.
  As of March 2019, Bitcoin abuse now require an API token, this can be
  obtained from their site and provided with either
  - `--api_token` parameter
  - the `BA_API_TOKEN` environment variable
  `BA_API_TOKEN=xxx ./update_bitcoin_abuse_list.pl`


## Notes
- The plugin scans an email and searches for strings that match the Bitcoin
  address format. These strings are then checked against a list of known
- Run `update_bitcoin_abuse_list.pl -h` to see options for updating the
  data
- The `update_bitcoin_abuse_list.pl` script can take some time to run as
  the source API employs rate-limiting


## License
- See included license file - Released under this license to be as
  compatible with SpamAssassin as possible


## Dependencies
- Spamassassin
- Perl Modules
  - LWP::UserAgent
  - JSON
  - Getopt::Long
  - File::Copy
  - URI


## Potential Issues
- It's possible that certain strings could be accidentally matched, but
  this should be a small chance.
- If you have a busy system and there seems to be increased disk IO,
  perhaps move the abuse file list to /dev/shm or some other ramdisk device.
  At present the data is small so system caching should overcome any issues
  like this at the moment.


## Future work
- The list is stored locally as plain text and searched sequentially when the
  email is scanned. Presently the list is small so this won't add too much
  overhead, potentially change this to use a database such as SQLite if the
  data becomes vast.


## Acknowledgements
- Thanks to the creators and maintainers of https://www.bitcoinabuse.com/


## Changelog
- 2023-03-28:: 1.05  :: If temporary errors were encountered during list
                        update but the list was eventually updated
                        succesfully, show a message indicating such.
                        Changes to update script only.
- 2023-01-31:: 1.04  :: Add retry delay multiplier on update script
                        Arguments on update script have changed
- 2019-08-10:: 1.03  :: Add further debug on HTTP error
- 2019-08-10:: 1.02  :: Warn HTTP error regardless of verbosity
- 2019-06-22:: 1.01  :: Add BA_API_TOKEN Environment Variable to be used
                        to supply API token.
                        Add Stricter checks on returned HTTP response
- 2019-03-28:: 1.00  :: Add in API use of API tokens when getting data.
                        Increment to 1.x major version
- 2018-12-20:: 0.02  :: Add per-message reporting showing the addresses
                        detected
- 2018-11-26:: 0.01  :: First Release


## Author
- Alasdair Keyes - https://www.akeyes.co.uk/
