#!/usr/bin/env perl

use strict;
use warnings;
use FindBin qw($Bin);
use lib "$Bin/../lib/";

use Mail::SpamAssassin;
use Test::More tests => 3;

my $sa = Mail::SpamAssassin->new();

require_ok('Mail::SpamAssassin::Plugin::BitcoinAbuse');

my $plugin = Mail::SpamAssassin::Plugin::BitcoinAbuse->new($sa);
isa_ok($plugin, 'Mail::SpamAssassin::Plugin');

my $conf = $sa->{ conf };

ok(ref($conf->{ eval_plugins }{ bitcoin_abuse_check }) eq 'Mail::SpamAssassin::Plugin::BitcoinAbuse', 'bitcoin_abuse_check registered with SA');
