#!/usr/bin/env perl

use strict;
use warnings;
use FindBin qw($Bin);
use lib "$Bin/../lib/";

use Mail::SpamAssassin;
use Mail::SpamAssassin::Plugin::BitcoinAbuse;
use Mail::SpamAssassin::PerMsgStatus;
use Test::More;

my $plugin_rules = {
    'BITCOIN_ABUSE_1'   => {
        plugin_sub  => 'bitcoin_abuse_check',
        config      => {
            rule_name   => 'BITCOIN_ABUSE_1',
            score       => 1,
            description => 'Email body contained known abuse bitcoin address',
        },
        plugin_opts => [
            {
                key     => 'minimum_reports_required',
                value   => '1',
                line    => 1,
                conf    => undef,
                user_config => 0,
            },
            {
                key     => 'bitcoin_abuse_list_file',
                value   => "$Bin/test_files/0003-bitcoin_abuse_list.txt",
                line    => 1,
                conf    => undef,
                user_config => 0,
            },
        ],
    }
};

my $tests = [
    {
        rule_name               => 'BITCOIN_ABUSE_1',
        name                    => 'spam',
        email_file              => "$Bin/test_files/0004-sample-spam.txt",
        expected_return_code    => 1,
        expected_report_hash    => {
            BITCOIN_ABUSE_1 => 1
        }
    },
    {
        rule_name               => 'BITCOIN_ABUSE_1',
        name                    => 'ham',
        email_file              => "$Bin/test_files/0004-sample-ham.txt",
        expected_return_code    => 0,
        expected_report_hash    => {}
    },    
];

# Helper functions
sub load_email {
    my $filename = shift;
    open(my $fh, '<', $filename)
    || die "Failed to load email example: $!";

    my $email_text = join('', <$fh>);
    close $fh;
    return $email_text;
}

sub build_spamassassin_instance {
    my $sa = Mail::SpamAssassin->new();
}

sub load_plugin_instance {
    my $sa = shift;
    my $conf = shift;
    my $valid_opts = shift;
    my $plugin = Mail::SpamAssassin::Plugin::BitcoinAbuse->new($sa);

    foreach my $valid_opt (@$valid_opts) {
        $valid_opt->{ conf } = $conf;
        $plugin->parse_config($valid_opt);
    }
    return $plugin;
}

sub inject_config_parameters {
    my $pms = shift;
    my $conf = shift;
    my $config_data = shift;

    my $rule_name = $config_data->{ rule_name };

    $pms->{ current_rule_name } = $rule_name;
    $conf->{ scores }{ $rule_name } = 1;
    $conf->{ descriptions }{ $rule_name } = 'Email body contained known abuse bitcoin address';
}

# Cycle through each $tests test, loading data from $plugin_rules and testing mail
{
    foreach my $test (@$tests) {
        my $test_name   = $test->{ name };
        my $rule_name   = $test->{ rule_name };
        my $rule_config = $plugin_rules->{ $rule_name };
        my $email_text  = load_email($test->{ email_file });
        my $sa          = build_spamassassin_instance();
        my $conf        = $sa->{ conf };
        my $plugin      = load_plugin_instance($sa, $conf, $rule_config->{ plugin_opts });
        my $message     = $sa->parse($email_text);
        my $pms         = Mail::SpamAssassin::PerMsgStatus->new(
                                $sa,
                                $message
                            );
        
        inject_config_parameters($pms, $conf, $rule_config->{ config });
        my $function = $rule_config->{ plugin_sub };
        ok(
            $plugin->$function($pms, $message->get_body()) == $test->{ expected_return_code },
            "Function:$function() Rule:$rule_name Test:$test_name return value $test->{ expected_return_code }"
        );

        is_deeply(
            $pms->get_names_of_tests_hit_with_scores_hash(),
            $test->{ expected_report_hash },
            "Function:$function() Rule:$rule_name Test:$test_name reports spam status succesfully"
        );
    }
}

done_testing();