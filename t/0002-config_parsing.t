#!/usr/bin/env perl

use strict;
use warnings;
use FindBin qw($Bin);
use lib "$Bin/../lib/";

use Mail::SpamAssassin;
use Test::More;
use Mail::SpamAssassin::Plugin::BitcoinAbuse;

my $sa = Mail::SpamAssassin->new();
my $plugin = Mail::SpamAssassin::Plugin::BitcoinAbuse->new($sa);
my $conf = $sa->{ conf };

my $minimum_reports_required_sets = [
    {
        opts => {
            key     => 'minimum_reports_required',
            value   => '1',
            line    => 1,
            conf    => $conf,
            user_config => 0,
        },
        expected => '1',
    },
    {
        opts => {
            key     => 'minimum_reports_required',
            value   => '',
            line    => 1,
            conf    => $conf,
            user_config => 0,
        },
        expected => '1',
    },
    {
        opts => {
            key     => 'minimum_reports_required',
            value   => '500000',
            line    => 1,
            conf    => $conf,
            user_config => 0,
        },
        expected => '500000',
    },
        {
        opts => {
            key     => 'minimum_reports_required',
            value   => 'asd',
            line    => 1,
            conf    => $conf,
            user_config => 0,
        },
        expected => '1',
    },
    {
        opts => {
            key     => 'minimum_reports_required',
            value   => undef,
            line    => 1,
            conf    => $conf,
            user_config => 0,
        },
        expected => '1',
    },

    {
        opts => {
            key     => 'bitcoin_abuse_list_file',
            value   => '/tmp/bitcoin_abuse_list.txt',
            line    => 1,
            conf    => $conf,
            user_config => 0,
        },
        expected => '/tmp/bitcoin_abuse_list.txt',
    },
    {
        opts => {
            key     => 'bitcoin_abuse_list_file',
            value   => '',
            line    => 1,
            conf    => $conf,
            user_config => 0,
        },
        expected => '/etc/spamassassin/bitcoin_abuse_list.txt',
    },
    {
        opts => {
            key     => 'bitcoin_abuse_list_file',
            value   => undef,
            line    => 1,
            conf    => $conf,
            user_config => 0,
        },
        expected => '/etc/spamassassin/bitcoin_abuse_list.txt',
    },
];

foreach my $minimum_reports_required_data (@$minimum_reports_required_sets) {
    my $opts = $minimum_reports_required_data->{ opts };
    my $expected = $minimum_reports_required_data->{ expected };

    is($plugin->parse_config($opts), 1, "'$opts->{ key }' parse_config() returns true");
    my $printable_value = defined($opts->{ value })
        ? "'$opts->{ value }'"
        : "undef";
    is($conf->{ bitcoin_abuse }{ $opts->{ key } }, $expected, "Config '$opts->{ key }' = " . $printable_value . " parsed correctly as '$expected'");
}

done_testing();
