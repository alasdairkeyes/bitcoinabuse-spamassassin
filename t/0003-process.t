#!/usr/bin/env perl

use strict;
use warnings;
use FindBin qw($Bin);
use lib "$Bin/../lib/";

use Mail::SpamAssassin;
use Mail::SpamAssassin::PerMsgStatus;
use Mail::SpamAssassin::Plugin::BitcoinAbuse;
use Array::Compare;
use Test::More;
use Test::Exception;
use Test::MockObject;

my $sa = Mail::SpamAssassin->new();
my $plugin = Mail::SpamAssassin::Plugin::BitcoinAbuse->new($sa);
my $conf = $sa->{ conf };

my $valid_opts = [
    {
        key     => 'minimum_reports_required',
        value   => '1',
        line    => 1,
        conf    => $conf,
        user_config => 0,
    },
    {
        key     => 'bitcoin_abuse_list_file',
        value   => "$Bin/test_files/0003-bitcoin_abuse_list.txt",
        line    => 1,
        conf    => $conf,
        user_config => 0,
    },
];

foreach my $valid_opt (@$valid_opts) {
    $plugin->parse_config($valid_opt);
}

{
    my $path = $plugin->default_list_filename();
    is($path, '/etc/spamassassin/bitcoin_abuse_list.txt', 'default_list_filename() returns OK');
}

{
    my $fh1 = $plugin->_get_abuse_list_file_handle("$Bin/test_files/0003-bitcoin_abuse_list.txt");
    is(ref($fh1), 'GLOB', '_get_abuse_list_file_handle() returns GLOB Filehandle');
    close($fh1);
}

{
    throws_ok { $plugin->_get_abuse_list_file_handle("/path/doesnt/exist") }
        qr/bitcoin_abuse Failed to open file/,
        '_get_abuse_list_file_handle() throws exceptio when failure to open file';
}

my $array_compare = Array::Compare->new();
{
    my @found_bitcoin_addresses = $plugin->_extract_bitcoin_strings([
        "Hello Joe,",
        "Send me some bitcoin to this address bc1aaaaaaaaaaaaaaaaaaaaaaaaaa",
        "Maybe also send it to 1aaaaaaaaaaaaaaaaaaaaaaaaaaaa and perhaps 3aaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        "Thanks"
    ]);

    ok(
        $array_compare->compare(
            \@found_bitcoin_addresses,
            [
                'bc1aaaaaaaaaaaaaaaaaaaaaaaaaa',
                '1aaaaaaaaaaaaaaaaaaaaaaaaaaaa',
                '3aaaaaaaaaaaaaaaaaaaaaaaaaaaa'
            ]
        ),
        '_extract_bitcoin_strings() returns array data with bitcoin addresses'
    );
}

{
    my @found_bitcoin_addresses = $plugin->_extract_bitcoin_strings([
        "Hello Joe,",
        "This is a regular email",
        "Thanks"
    ]);

    ok(
        $array_compare->compare(\@found_bitcoin_addresses, []),
        '_extract_bitcoin_strings() returns empty array on regular email'
    );
}

{
    my $addresses_in_email = [];

    my $abuse_file = "$Bin/test_files/0003-bitcoin_abuse_list.txt";
    open(my $fh, '<', $abuse_file)
        || die "Failed to open '$abuse_file': $!";

    my $found_bitcoin_addresses = $plugin->_search_for_addresses_in_email(
        $fh,
        $addresses_in_email
    );
    is_deeply(
        $found_bitcoin_addresses,
        {},
        '_search_for_addresses_in_email() returns hashref'
    );
}

{
    my $addresses_in_email = [
        'bc1aaaaaaaaaaaaaaaaaaaaaaaaaa',
        '1aaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    ];

    my $abuse_file = "$Bin/test_files/0003-bitcoin_abuse_list.txt";
    open(my $fh, '<', $abuse_file)
        || die "Failed to open '$abuse_file': $!";

    my $found_bitcoin_addresses = $plugin->_search_for_addresses_in_email(
        $fh,
        $addresses_in_email
    );
    is_deeply(
        $found_bitcoin_addresses,
        {
          '1aaaaaaaaaaaaaaaaaaaaaaaaaaaa' => '6',
          'bc1aaaaaaaaaaaaaaaaaaaaaaaaaa' => '1'
        },
        '_search_for_addresses_in_email() returns hashref with addresses and scores'
    );
}

{
    is($plugin->_report_count(
        {
          '1aaaaaaaaaaaaaaaaaaaaaaaaaaaa' => '6',
          'bc1aaaaaaaaaaaaaaaaaaaaaaaaaa' => '1'
        }
        ),
        7,
        '_report_count() processes hashref correctly'
    );
}

{
    my $description = "Test description for mail processing";
    my $rulename = "BITCOIN_ABUSE_1";
    
    my $pms_mock = Test::MockObject->new();
    $pms_mock->set_true('got_hit');
    
    my $d = $plugin->_update_report_description($pms_mock, $description, $rulename,         {
          '1aaaaaaaaaaaaaaaaaaaaaaaaaaaa' => '6',
          'bc1aaaaaaaaaaaaaaaaaaaaaaaaaa' => '1'
        });
    
    is($pms_mock->called('got_hit'), 1, '_update_report_description() calls got_hit()');
}

done_testing();