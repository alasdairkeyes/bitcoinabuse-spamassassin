#!/usr/bin/env perl

use strict;
use warnings;
use Fcntl ':flock';
use File::Copy;
use Getopt::Long;
use JSON;
use LWP::UserAgent;
use Mail::SpamAssassin::Plugin::BitcoinAbuse;

my $report_base_url = 'https://www.bitcoinabuse.com/api/reports/distinct';

my $default_output_file = Mail::SpamAssassin::Plugin::BitcoinAbuse->default_list_filename();
my $default_retry_limit = 5;
my $default_retry_delay_multiplier = 10;
my $default_http_timeout = 10;
my $default_request_delay = 2;
my $env_var_key = 'BA_API_TOKEN';

my $json = JSON->new->utf8(1);

my $VERSION = $Mail::SpamAssassin::Plugin::BitcoinAbuse::VERSION;

my $verbose = 0;
my $api_token = '';
my $output_file = $default_output_file;
my $retry_limit = $default_retry_limit;
my $retry_delay_multiplier = $default_retry_delay_multiplier;
my $http_timeout = $default_http_timeout;
my $request_delay = $default_request_delay;
my $help = 0;

GetOptions (
    "verbose"                   => \$verbose,
    "api_token=s"               => \$api_token,
    "output=s"                  => \$output_file,
    "retry_limit=i"             => \$retry_limit,
    "retry_delay_multiplier=i"  => \$retry_delay_multiplier,
    "timeout=i"                 => \$http_timeout,
    "delay=i"                   => \$request_delay,
    "help"                      => \$help,
);

help()
    if ($help);

sub help {
    print "USAGE\n";
    print <<EOF;
$0 [ --verbose --api_token=XXX --output=$default_output_file --retry_limit=$default_retry_limit --retry_delay_multiplier=$default_retry_delay_multiplier --timeout=$default_http_timeout --delay=$default_request_delay --help ]

Version: $VERSION
Sync list of abusive bitcoin addresses from https://www.bitcoinabuse.com/
This can take some time to run as it downloads data in chunks, as the database increases, the run-time will increase.

-v, --verbose               Print debug information
-a, --api_token             The Bitcoin abuse API to access the service. Required.
-o, --output                Location to output the data (Default: $default_output_file
--retry_limit               Give up after this many failed attempts to get data (Default: $default_retry_limit)
--retry_delay_multiplier    When an error occurs fetch the API, back off for this number of second multiplied by the number of retries that have been attempted (Default: $default_retry_delay_multiplier)
-t, --timeout               Timeout in seconds for HTTP requests (Default: $default_http_timeout)
-d, --delay                 Wait this many seconds between getting data to avoid rate-limiting (Default: $default_request_delay)
-h, --help                  Display this help information
EOF
    exit 0;

}

sub debug {
    return unless ($verbose);
    warn join(' ', @_) . "\n";
}

die "Detected another update process already running"
    unless flock( DATA, LOCK_EX|LOCK_NB );

my $output_file_temp = $output_file . '.tmp';

my $retry_count = 0;

# FIXME: Output as types

my $address_re = "/\b(bc1|[13])[a-zA-HJ-NP-Z0-9]{25,39}\b/gi";

my $ua = LWP::UserAgent->new(
    agent   => "bitcoinabuse-spamassassin $VERSION sync",
    timeout => $http_timeout
);

debug("File to write to $output_file");
debug("Writing to temfile $output_file_temp");

open (my $fh, '>', $output_file_temp)
    || die "Failed to open temporary_file " . $output_file_temp . ": $!";

$api_token ||= $ENV{$env_var_key};

die "No API token provided"
    unless $api_token;

my $next_page_url = $report_base_url;
my $page = 1;

while ($retry_count < $retry_limit) {
    debug("Calling $report_base_url - Page $page");
    my $url = URI->new($report_base_url);
    $url->query_form(api_token => $api_token, page => $page);
    my $response = $ua->get($url);
    debug("Sleeping for $request_delay seconds");
    sleep $request_delay;

    if ($response->is_success) {
        die "Did not get JSON API response, check your API key is valid"
            unless ($response->header('content-type') =~ m#^application/json\b#);

        my $json_response = $response->decoded_content;
        my $data = $json->decode($json_response);

        $next_page_url = $data->{ next_page_url };

        push(my @addresses, map {
            join(',', $_->{ address }, $_->{ count })
            } @{$data->{ data }});

        print $fh join("\n", @addresses) . "\n";

        if ($next_page_url) {
            ++$page;
        } else {
            close $fh;
            debug("Reached last page of data.");
            debug("Moving temp file $output_file_temp to $output_file");
            move($output_file_temp, $output_file)
                || die "Failed to copy temporary file $output_file_temp to $output_file: $!";

            if ($retry_count) {
                warn "Errors were encountered but list updated successfully";
            }
            exit;
        }
    } else {
        warn "Failed to get $next_page_url (page: $page): " . $response->status_line;

        my $error_delay = $retry_delay_multiplier * $retry_count;
        debug("Sleeping for $error_delay seconds after error");
        sleep $error_delay;

        $retry_count++;
    }
}
die "Received $retry_count errors whilst getting data, stopping retrieval"

__DATA__
